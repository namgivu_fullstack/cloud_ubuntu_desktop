--- build it
gg droplet with ubuntu desktop

now avail. and working guide ref. https://docs.digitalocean.com/tutorials/droplet-desktop/
start w/ digitalocean ubuntu droplet, install xrpd, install ubuntu-desktop, adduser a notroot one

CAUTION Ubuntu Desktop required as 22.04 minimum or display resolution be too small and cannot change when remmina-connect

remmina seems be best to connect to your ubuntu-desktop droplet  ref. https://linuxhint.com/best_remote_desktops_linux_mint/
install remmina                                                  ref. https://remmina.org/how-to-install-remmina/#snap

--- result
== resolution too small?
remmina has the option of dynamic resolution which will change the resolution on your windows resized

== video 
![230626_remotedesktop_w_remmina.gif](doc/230626_remotedesktop_w_remmina.gif)

== snapshot
![run remmina to enter ip to connect](doc/230600_connect_w_remmina.png)
![login w/ your ubuntu user+pass to see the wallpaper!](doc/230601_can_see_ubuntudesktop_wallpaper.png)
